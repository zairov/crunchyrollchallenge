//
//  PreviewTableCell.swift
//  CrunchyRollChallenge
//
//  Created by Vali Zairov on 4/1/16.
//  Copyright © 2016 Vali Zairov. All rights reserved.
//

import UIKit

class PreviewTableCell: UITableViewCell{
    
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
}
