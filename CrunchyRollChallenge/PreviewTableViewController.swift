//
//  PreviewTableViewController.swift
//  CrunchyRollChallenge
//
//  Created by Vali Zairov on 4/1/16.
//  Copyright © 2016 Vali Zairov. All rights reserved.
//

import UIKit

class PreviewTableViewController: UITableViewController {
    
    var array = [Model]()
    
    override func viewDidLoad() {
        Model.getData { (modelData, jsonErr) -> () in
            if jsonErr == nil{
                self.array = modelData!
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    self.tableView.reloadData()
                })
            }else if jsonErr == Model.JSONError.ErrorParsing{
                print("Error parsing the data")
            }else {
                print("Network Error")
            }
        }
    }
    
    
    //MARK: TableView
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "previewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PreviewTableCell
        let item = self.array[indexPath.row]
        if !item.thumbURL!.isEmpty{
            let imageTask = NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: item.thumbURL!)!, completionHandler: { (imageData: NSData?, imageResponse: NSURLResponse?, imageError:NSError?) -> Void in
                if imageData != nil{
                    let img = UIImage(data: imageData!)
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        cell.thumbImageView.image = img
                    })
                }
            })
            
            imageTask.resume()
        }else{
            cell.thumbImageView.image = item.thumbImage
        }
        cell.captionLabel.text = item.caption
        
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("detailSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "detailSegue") {
            let destinationNavigationController = segue.destinationViewController as! UINavigationController
            let vc = destinationNavigationController.topViewController as! ViewController
            let tempItem = self.array[self.tableView.indexPathForSelectedRow!.row]
            vc.imgURL = tempItem.fullImageURL!
        }
    }
}
