//
//  ViewController.swift
//  CrunchyRollChallenge
//
//  Created by Vali Zairov on 4/1/16.
//  Copyright © 2016 Vali Zairov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var imgURL : String?
    var isFullScreen = false
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var detailImageView: UIImageView!
//    @IBOutlet weak var detailWidth: NSLayoutConstraint!
//    @IBOutlet weak var detailHeight: NSLayoutConstraint!
    @IBOutlet weak var menuBarItem: UIBarButtonItem!
    @IBOutlet weak var progressActivity: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //menu
        if let revealVC = self.revealViewController() {
            menuBarItem.target = revealVC
            menuBarItem.action = "revealToggle:"
            self.view.addGestureRecognizer(revealVC.panGestureRecognizer())
        }
        
        // constraints
        let imgConstraints = self.detailImageView.constraints
        self.detailImageView.removeConstraints(imgConstraints)
        let constraints: [NSLayoutConstraint] = [
            self.detailImageView.leadingAnchor.constraintEqualToAnchor(self.scrollView.leadingAnchor),
            self.detailImageView.trailingAnchor.constraintEqualToAnchor(self.scrollView.trailingAnchor),
            self.detailImageView.topAnchor.constraintEqualToAnchor(self.scrollView.topAnchor),
            self.detailImageView.bottomAnchor.constraintEqualToAnchor(self.scrollView.bottomAnchor)
        ]
        NSLayoutConstraint.activateConstraints(constraints)
        
        //fullscreen mode
        let gesture = UITapGestureRecognizer(target: self, action: "fullScreenMode:")
        self.detailImageView.addGestureRecognizer(gesture)

        
        if let imgURL = imgURL {
            progressActivity.startAnimating()
            let imageTask = NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: imgURL)!, completionHandler: { (data:NSData?, response: NSURLResponse?, err:NSError?) -> Void in
                if data != nil{
                    let img = UIImage(data: data!)
                    NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                        self.detailImageView.image = img
                        self.progressActivity.stopAnimating()
                    })
                }
            })

            imageTask.resume()
        }
    }

    
    func fullScreenMode(sender:UITapGestureRecognizer){
        if isFullScreen{
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            UIApplication.sharedApplication().statusBarHidden = false
            isFullScreen = false
        }else{
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            UIApplication.sharedApplication().statusBarHidden = true
            isFullScreen = true
        }
    }
}

