//
//  Model.swift
//  CrunchyRollChallenge
//
//  Created by Vali Zairov on 4/1/16.
//  Copyright © 2016 Vali Zairov. All rights reserved.
//

import Foundation
import UIKit

class Model {
    
    enum JSONError: ErrorType{
        case ErrorParsing
        case NetworkError
    }
    
    var caption: String?
    var thumbImage: UIImage?
    var thumbURL: String?
    var fullImage: UIImage?
    var fullImageURL: String?
    
    init(caption: String, thumbURL: String, fullImageURL: String){
        self.caption = caption
        self.thumbURL = thumbURL
        self.fullImageURL = fullImageURL
        self.thumbImage = UIImage(named: "placeholder.png")
        self.fullImage  = UIImage(named: "placeholder.png")
    }
    
    class func getData(completion: (modelData: [Model]?, jsonErr: JSONError?)->()){
        let url = NSURL(string: "http://www.crunchyroll.com/mobile-tech-challenge/images.json")
        var modelArr = [Model]()
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data:NSData?, response: NSURLResponse?, error:NSError?) in
            if (data != nil){
                do {
                    let object = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    if let dictionary = object as? [AnyObject] {
                        for item in dictionary {
                            var thumb = ""
                            var original = ""
                            var caption = ""
                            if let caption2 = item["caption"] as? String {
                                caption = caption2
                            }
                            if let thumb2 = item["thumb"] as? String {
                                thumb = thumb2
                            }
                            if let original2 = item["original"] as? String {
                                original = original2
                            }
                            
                            let tempModel = Model(caption: caption, thumbURL: thumb, fullImageURL: original)
                            
                            modelArr.append(tempModel)
                        }
                        completion(modelData: modelArr, jsonErr: nil)
                    }
                } catch {
                    completion(modelData: nil, jsonErr: JSONError.ErrorParsing)
                    //print("Error parsing the data")
                }
            }else{
                completion(modelData: nil, jsonErr: JSONError.NetworkError)
                //print("Network Error")
            }
        }
        
        
        task.resume()
            
    }
}
